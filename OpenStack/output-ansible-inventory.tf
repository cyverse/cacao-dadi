resource "local_file" "ansible-inventory" {
    count = var.power_state == "active" ? 1 : 0

    content = templatefile("${path.module}/hosts.yml.tmpl",
    {
        dadi_instance_ips = openstack_compute_floatingip_associate_v2.os_floatingips_associate.*.floating_ip
        dadi_instance_names = openstack_compute_instance_v2.os_instances.*.name # we could use this instead of an generically generated index name
        dadi_user = local.real_username
        dadi_enable_auto_cli = var.run_dadi_cli
        dadi_cli_parameters = var.dadi_cli_parameters
        dadi_cli_use_workqueue = var.dadi_cli_use_workqueue
        dadi_enable_workqueue_factory = var.run_workqueue_factory
        dadi_project_name = var.workqueue_project_name
        dadi_workqueue_password = var.workqueue_password
    })
    filename = "${path.module}/ansible/hosts.yml"
}

resource "null_resource" "ansible-execution" {
    count = var.power_state == "active" ? 1 : 0

    triggers = {
        always_run = "${timestamp()}"
    }

    provisioner "local-exec" {
        command = "ansible-galaxy install -r requirements.yml -f"
        working_dir = "${path.module}/ansible"
    }

    provisioner "local-exec" {
        command = "ANSIBLE_HOST_KEY_CHECKING=False ANSIBLE_SSH_PIPELINING=1 ANSIBLE_CONFIG=ansible.cfg ansible-playbook -i hosts.yml playbook.yml"
        working_dir = "${path.module}/ansible"
    }

    depends_on = [
        local_file.ansible-inventory
    ]
}
