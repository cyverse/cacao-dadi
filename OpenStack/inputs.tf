variable "username" {
  type = string
  description = "username"
}

variable "project" {
  type = string
  description = "project name"
}

variable "region" {
  type = string
  description = "string, openstack region name; default = IU"
  default = "IU"
}

variable "instance_name" {
  type = string
  description = "name of instance"
}

variable "instance_count" {
  type = number
  description = "number of instances to launch"
  default = 1
}

variable "image" {
  type = string
  description = "string, image id; image will have priority if both image and image name are provided"
  default = ""
}

variable "image_name" {
  type = string
  description = "string, name of image; image will have priority if both image and image name are provided"
  default = ""
}

variable "flavor" {
  type = string
  description = "flavor or size of instance to launch"
  default = "m1.tiny"
}

variable "keypair" {
  type = string
  description = "keypair to use when launching"
  default = ""
}

variable "power_state" {
  type = string
  description = "power state of instance"
  default = "active"
}

variable "user_data" {
  type = string
  description = "cloud init script"
  default = ""
}

variable "fip_associate_timewait" {
  type = string
  description = "number, time to wait before associating a floating ip in seconds; needed for jetstream; will not be exposed to downstream clients"
  default = "30s"
}

variable "run_dadi_cli" {
  default = false
  type = bool
}

variable "dadi_cli_parameters" {
  default = ""
  type = string
}

variable "dadi_cli_use_workqueue" {
  default = false
  type = bool
}

variable "run_workqueue_factory" {
  default = false
  type = bool
}

variable "workqueue_project_name" {
  default = ""
  type = string
}

variable "workqueue_password" {
  default = ""
  type = string
}

variable "network" {
  type = string
  description = "network to use for vms"
  default = "auto_allocated_network"
}